//
//  ViewController.swift
//  CalC
//
//  Created by Thanisorn Jundee on 1/18/2560 BE.
//  Copyright © 2560 Thanisorn Jundee. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var textResult: UILabel!
    //MARK : Define Numpad
    @IBAction func NumC(_ sender: Any) {
        self.clResult()
    }
    @IBAction func Num0(_ sender: Any) {
       self.assignNum(n: "0")     }
    @IBAction func Num1(_ sender: Any) {
        self.assignNum(n: "1")    }
    @IBAction func Num2(_ sender: Any) {
        self.assignNum(n: "2")    }
    @IBAction func Num3(_ sender: Any) {
        self.assignNum(n: "3")    }
    @IBAction func Num4(_ sender: Any) {
        self.assignNum(n: "4")    }
    @IBAction func Num5(_ sender: Any) {
        self.assignNum(n: "5")    }
    @IBAction func Num6(_ sender: Any) {
        self.assignNum(n: "6")    }
    @IBAction func Num7(_ sender: Any) {
        self.assignNum(n: "7")    }
    @IBAction func Num8(_ sender: Any) {
        self.assignNum(n: "8")    }
    @IBAction func Num9(_ sender: Any) {
        self.assignNum(n: "9")    }
    @IBAction func NumDot(_ sender: Any) {
        if Calcs.isEmpty(){
            Calcs.Setter(Set: "0.")
        }
        else{
            Calcs.addResmaster(textRe: ".")
        }
        self.notify()
    }
    @IBAction func NumPlus(_ sender: Any) {
        Calcs.addNum(operation: 1)
        Calcs.clearTextMas()
    }
    @IBAction func NumInverse(_ sender: Any) {
        Calcs.inverse()
        self.notify()
    }
    @IBAction func NumEqu(_ sender: Any) {
        Calcs.doOperation()
        self.notify()
        Calcs.Clear()
    }
    @IBAction func NumMinus(_ sender: Any) {
        Calcs.addNum(operation: 2)
        Calcs.clearTextMas()
    }
    @IBAction func NumMulti(_ sender: Any) {
        Calcs.addNum(operation: 3)
        Calcs.clearTextMas()
    }
    @IBAction func NumDivide(_ sender: Any) {
        Calcs.addNum(operation: 4)
        Calcs.clearTextMas()
    }
    //MARK: END NUMPAD
    var Calcs = Calculate()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.notify()
    }
    func notify()
    {
        textResult.text = Calcs.ResultMaster
    }
    func clResult()
    {
        Calcs.Clear()
        self.notify()
    }
    func assignNum(n:String)
    {
        if Calcs.isEmpty() {
            Calcs.Setter(Set: n)
        }else{
            Calcs.addResmaster(textRe: n)
        }
        self.notify()
    }

}
class Calculate {
    
    var Operation:Int
    var Num1:Double?
    var Num2:Double?
    var ResultMaster:String
    var Couting:Bool
    init()
    {
        Operation = 0
        Num1 = nil
        Num2 = nil
        ResultMaster = "0"
        Couting = false
    }
    func Clear()
    {
        self.Operation = 0
        self.Num1 = nil
        self.Num2 = nil
        ResultMaster = "0"
        Couting = false
    }
    func addResmaster(textRe:String)
    {
        ResultMaster.append(textRe)
    }
    func clearTextMas()
    {
        ResultMaster = "0"
    }
    func Setter(Set:String)
    {
        ResultMaster = Set
    }
    func isEmpty() -> Bool {
        if ResultMaster == "0"
        {
            return true
        }
        else
        {
            return false
        }
    }
    func inverse()
    {
        let Decision = Double(ResultMaster)
        
        if floor(Decision!) == Decision!
        {
            let Current:Int = Int(Decision!)
            let D = (Current * (-1))
            self.Setter(Set: String(D))
        }else{
            let Current:Double = Double(Decision!)
            let D = (Current * (-1))
            self.Setter(Set: String(D))
        }
    }
    func assignOperation(op:Int) {
        Operation = op
        
    }
    func addNum(operation:Int){
        if Couting {
            if Num2 == nil {
                Num2 = Double(ResultMaster)
                self.doOperation()
                self.assignOperation(op: operation)
            }
            else{
                doOperation()
                self.assignOperation(op: operation)
                Num1 = Double(ResultMaster)
            }
        }else{
                Num1 = Double(ResultMaster)
            self.assignOperation(op: operation)
            Couting = true
        }
    }
    func clearNum()
    {
        Num2 = nil
        Num1 = nil
    }
    func doOperation ()
    {
        if Num2 == nil
        {
            Num2 = Double(ResultMaster)
        }
        switch Operation {
        case 1:
            let r = Num1! + Num2!
            var dim:Double!
            if floor(r) == r{
            ResultMaster = String(Int(r))
                dim = r
            }else{
                ResultMaster = String(r)
                dim = r
            }
            clearNum()
            self.Num1 = dim
            break
        case 2:
            let r = Num1! - Num2!
            var dim:Double!
            if floor(r) == r{
                ResultMaster = String(Int(r))
                dim = r
            }else{
                ResultMaster = String(r)
                dim = r
            }
            clearNum()
            self.Num1 = dim
            break
        case 3:
            let r = Num1! * Num2!
            var dim:Double!
            if floor(r) == r{
                ResultMaster = String(Int(r))
                dim = r
            }else{
                ResultMaster = String(r)
                dim = r
            }
            clearNum()
            self.Num1 = dim
            break
        case 4:
            let r = Num1! / Num2!
            var dim:Double!
            if floor(r) == r{
                ResultMaster = String(Int(r))
                dim = r
            }else{
                ResultMaster = String(r)
                dim = r
            }
            clearNum()
            self.Num1 = dim
            break
        default:
            break
        }
        Operation = 0
    }
}


